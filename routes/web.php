<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ContactController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Routes gestione dati form
Route::get('/', [ContactController::class,'create'])->name('home');
Route::post('/store', [ContactController::class,'store'])->name('contacts.store');

//Route view app
Route::get('/risposte', [PublicController::class,'answer'])->name('answers');
Route::get('/frontend', [PublicController::class,'front'])->name('front');
Route::get('/curruculum', [PublicController::class,'cv'])->name('cv');

Route::post('/map/store', [ShopController::class,'store'])->name('map.store');

Route::get('/map', [ShopController::class,'map'])->name('map');