<nav id="navbar" class="navbar navbar-expand-md bg-transparent transition d-flex justify-content-md-between justify-content-center px-5">
    
  <div>
    <img class="" height="70" id="navLogo" src="./media/Logo_brainpull.png" alt="Craft Beer Main Demo">
  </div>
   

  <div>  
    <button class="navbar-toggler text-center" type="button" data-toggle="collapse" data-target="#navDropdown" aria-controls="navDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-bars fa-2x text-light"></i>
    </button>

    <div class="collapse navbar-collapse" id="navDropdown">
      <ul class="navbar-nav mr-auto font-weight-bold lead ">
        <li class="nav-item">
          <a class="nav-link linkNav text-light" href="{{route('home')}}">Home </span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link linkNav text-light" href="{{route('answers')}}">Risposte</a>
        </li>
        <li class="nav-item">
          <a class="nav-link linkNav text-light" href="{{route('front')}}">Front-End</a>
        </li>
        <li class="nav-item">
          <a class="nav-link linkNav text-light" href="{{route('map')}}">Punti Vendita</a>
        </li>
        <li class="nav-item">
          <a class="nav-link linkNav text-light" href="{{route('cv')}}">Curriculum</a>
        </li>
        
      </ul>
     
    </div>
  </div>
</nav> 




