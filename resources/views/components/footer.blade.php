<footer class="bg-dark text-center text-lg-start">
    <!-- Grid container -->
    <div class="container p-4">
    <!--Grid row-->
    <div class="row justify-content-around">
        <ul class=" border-top px-5">
            <h5 class="p text_base mt-3 mb-4 text-light">I miei social!</h5>
            <li class="pl-2 list-inline-item mr-3"><a href="https://www.linkedin.com/in/dario-elefante/"><i class="fab fa-linkedin fa-2x text-light"></i></a></li>
            <li class="ml-2 list-inline-item mr-3"><a href="https://gitlab.com/dar.elefante"><i class="fab fa-gitlab fa-2x text-light"></i></a></li>
        </ul></div>
    <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgb(0, 0, 0)">
    © 2020 Copyright:
    <a class="text-light" href="https://mdbootstrap.com/">Dario Elefante</a>
    </div>
    <!-- Copyright -->
</footer>