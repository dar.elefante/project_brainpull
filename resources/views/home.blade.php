<x-layout>
    <x-slot name="title">Home</x-slot>

    

    <section>   

        {{-- Creo un form semplice senza action, method, csrf ne nulla. Axios in laravel porta con se il csrf di default --}}
        <div class="container-fluid mt-5 mb-0 p-5 newsletter">
            <form id="homepage">        
                <div class="form-group row d-flex">
                    <div class="col-12 col-md-6">
                        <label for="name">Nome</label>
                        <input type="text" class="form-control" id="nme" >   
                    </div>
                    <div class="col-12 col-md-6">
                        <label for="surname">Cognome</label>
                        <input type="text" class="form-control" id="surname">       
                    </div>
                </div>
            
                <div class="form-group row d-flex align-items-center">
                    <div class="col-12 col-md-4 my-2">
                        <label for="telephone">Telefono</label>
                        <input type="tel" class="form-control" id="telephone">
                    </div>
                    <div class="col-12 col-md-4">
                        <label for="email">email</label>
                        <input type="email" class="form-control" id="email">
                    </div>            
                    <div class="col-12 col-md-4">
                        <label for="age">età</label>
                        <input type="number" class="form-control" id="age">
                    </div>  
                </div>    
                
                <div class="form-group row d-flex">
                    <div class="col-12 col-md-8">
                    <label for="cv">CV</label>
                    <input type="file" class="form-control" id="cv" accept=".pdf">
                    </div> 
                </div>
                {{-- aggiungo un evento onclick che triggheri la funzione send in script.js --}}
                <div class="col-12 row text-right">
                    <button onclick="send()" type="button" class="btn btn-primary text-right">Invia</button>
                </div>    
            </form>
        </div>

    </section>


      


</x-layout>