<x-layout>
    <x-slot name=title>Map </x-slot>
<section>
    <div class="row mt-5">
        <div class="col-12">
            <h1 class="text-center font-weight-bold">Inserisci qui i Punti vendita</h1>
        </div>
    </div>

    <form class="container my-5 py-5" action="{{route('map.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="name">Nome PvD</label>
          <input type="text" name="name" class="form-control" id="name">
        </div>
        <div class="form-group">
            <label for="img">Carica immagine</label>
            <input type="file" name="img" class="form-control-file" id="img">
        </div>
        
        <div class="form-group">
            <b>Coordinates</b>
                
            <input type="text" class="form-control-file" name="lat" id="lat" size=12 value="">
            <input type="text" class="form-control-file" name="lon" id="lon" size=12 value="">
            
            
            <label for="addr">Cerca indirizzo</label>>
            <div id="search">
                <input type="text" class="form-control-file" name="addr" value="" id="addr" size="58" />
                <button type="button" class="btn btn-info" onclick="addr_search();">Search</button>

            <div id="results"></div>
            </div>
            
            <br>
            
            <div id="map"></div>
        </div>

        <div class="form-group">
          <label for="description">Example textarea</label>
          <textarea class="form-control" name="description" id="description" rows="3"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Aggiungi</button>
    </form>

    
   
    <div  id="mapid" style=" height: 180px;"
      breweries="{{$pvd}}">
      
    </div>


</section>


@push('gsap')

    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>    
    <script src="https://leaflet.github.io/Leaflet.markercluster/dist/leaflet.markercluster-src.js"></script>

    <script>
        
        function chooseAddr(lat1, lng1)
        {
           
            lat = lat1.toFixed(8);
            lon = lng1.toFixed(8);
            document.getElementById('lat').value = lat;
            document.getElementById('lon').value = lon;
           
        }
        
        function myFunction(arr)
        {
            var out = "<br />";
            var i;
            
            if(arr.length > 0)
            {
                for(i = 0; i < arr.length; i++)
                {
                    out += `<div class='address' title='Show Location and Coordinates' onclick='chooseAddr(${[arr[i].lat, arr[i].lon]})'> ${arr[i].display_name} </div>`;
                }
                document.getElementById('results').innerHTML = out;
            }
            else
            {
                document.getElementById('results').innerHTML = "Sorry, no results...";
            }
        }
        
        function addr_search()
        {
            var inp = document.getElementById("addr");
            var xmlhttp = new XMLHttpRequest();
            var url = "https://nominatim.openstreetmap.org/search?format=json&limit=5&q=" + inp.value;
            xmlhttp.onreadystatechange = function()
            {
                if (this.readyState == 4 && this.status == 200)
                {
                    var myArr = JSON.parse(this.responseText);
                    console.log(myArr)
                    myFunction(myArr);
                }
            };
            xmlhttp.open("GET", url, true);
            xmlhttp.send();
        }
    </script>    
    
    <script>
      let mapid = document.querySelector('#mapid')
      let breweries = JSON.parse(mapid.getAttribute('breweries'))
      let latlon = breweries.map(el => [el.lat, el.lon, el.name, el.description,el.img])

      let mymap = L.map('mapid').setView([latlon[0][0],latlon[0][1]], 13);
      console.log(mymap)
      
      L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoiZGFyaW9lbGVmYW50ZSIsImEiOiJja2p4Mmo2cWcwbzFmMnZsN2NuOGxzbXZ1In0.jcYV_OvImDtk4evVpdsVVA'
        }).addTo(mymap);

      let markers = L.markerClusterGroup();
      
      latlon.forEach(el => {
       
        let info = `
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="./storage/app/${el[4]}" alt="Card image cap">
                     <div class="card-body">
                        <h5 class="card-title">${el[2]}</h5>
                        <p class="card-text">${el[3]}</a>
                     </div>
                    </div>
                    `
        
        let marker = L.marker([el[0], el[1]])
        .bindPopup(info).openPopup()
        markers.addLayer(marker);

      });

      mymap.addLayer(markers);

      console.log(markers)
   
    </script>

@endpush

</x-layout>