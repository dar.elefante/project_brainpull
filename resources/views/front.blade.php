<x-layout>
  <x-slot name="title">Le Risposte</x-slot>

  <section style="min-height:50vh">

    <div class="form-group container my-5">
      <label for="customText">Input</label>
      <div class="row">
        <input type="text" class="form-control col-12 col-md-10" id="customText">
        <button onclick="print()" type="button" class="btn btn-primary">Submit</button>
      </div>
    </div>
    
    
    <div class="container mt-5">
      <div id="toAppend" class="row">
        
      </div>
    </div>

  </section>
  
  
  
  
 
 
  
  {{-- <div id="mapid" style="height: 180px;" pvd="{{$pvd}}"></div> --}}

  
  
  
  <!-- /GSAP -->
  
  @push('gsap')
  
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/gsap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/CSSRulePlugin.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/Draggable.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/EaselPlugin.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/MotionPathPlugin.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/PixiPlugin.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/TextPlugin.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/ScrollToPlugin.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/ScrollTrigger.min.js"></script>
  
  
  
  
  <script>
    let customText = document.querySelector('#customText')
    let toAppend = document.querySelector('#toAppend')
    
    function print(){
      
      let value = customText.value
      let split = value.split('')            
      sessionStorage.setItem("customtext", value)            
      let map = split.map(el => {
        return`<p class="test d-inline font-weight-bold h3">${el}</p>`
      })    
      let definitive = map.join('')         
      toAppend.innerHTML =  definitive
      value = ""
      gsap.from(".test", {duration: 1, stagger: 0.5, scale: 0, opacity:0})       
    }
    
    let sessionText = sessionStorage.getItem("customtext").split('').map(el => {                
      return`<p class="store d-inline font-weight-bold h3 text-info">${el}</p>`
    }).join('');        
    toAppend.innerHTML = sessionText             
    
    gsap.from(".store", {duration: 1, stagger: 0.5, scale: 0, opacity:0})
    
    
  </script>
  
  {{-- leaflet --}}
  
  
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>    
  <script src="https://leaflet.github.io/Leaflet.markercluster/dist/leaflet.markercluster-src.js"></script>
  
  <script>
    let mapid = document.querySelector('#mapid')
    let breweries = JSON.parse(mapid.getAttribute('pvd'))
    let latlon = breweries.map(el => [el.lat, el.lon, el.name, el.description,el.img])
    
    let mymap = L.map('mapid').setView([latlon[0][0],latlon[0][1]], 13);
    
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1IjoiZGFyaW9lbGVmYW50ZSIsImEiOiJja2p4Mmo2cWcwbzFmMnZsN2NuOGxzbXZ1In0.jcYV_OvImDtk4evVpdsVVA'
    }).addTo(mymap);
    
    
    let greenIcon = L.icon({
      iconUrl: '/media/marker2.png',        
      iconSize:     [45, 50], // size of the icon       
      iconAnchor:   [20, 25], // point of the icon which will correspond to marker's location       
      popupAnchor:  [5, -26] // point from which the popup should open relative to the iconAnchor
    }); 
    
    let markers = L.markerClusterGroup();
    
    latlon.forEach(el => {
      
      let info = `
      <img src="./storage/${el[4]}" alt="">
      <p class="lead">${el[2]}</p>
      <p class="text-truncate">${el[3]}</p>
      `
      
      
      
      let marker = L.marker([el[0], el[1]],{icon: greenIcon})
      .bindPopup(info).openPopup()
      markers.addLayer(marker);
      
    });
    
    mymap.addLayer(markers);
    
    console.log(markers)
    
  </script>
  
  
  
  @endpush    
  
  
</x-layout>