<x-layout>
    <x-slot name="title">Le Risposte</x-slot>

    


    <div class="container-fluid my-5">

        <div class="col-12 text-center">
            <h2 class="font-weight-bold h1">Back-end</h2>
        </div>

        <div data-aos="fade-up" class="row mt-5s">
            
            <div class=" col-12 col-md-8 shadow bg-light mx-auto border border-dark rounded p-3">
                <p class="lead font-weight-bold h3">Illustrare tutte le contromisure che possono portare alla prevenzione di un attacco XSS orientato al Sessio Hijacking su stack LAMP</p>
                <hr>
                <p class="lead h4">
                    
                    Il primo provvedimento per tutelarsi da un attacco cross-site rivolto al dirottamento di sessione è l'abilitazione del protocollo di trasferimento protetto da connessione criptata. In pratica si tratta di abilitare per il proprio applicativo web un protocollo di trasferimento HTTPS, protetto da un layer SSL sottostante che si occupa di criptare la comunicazione tra client e server generando token di sessione a breve termine. In genere i servizi di hosting offrono la possibilità di acquistare ed abilitare un certificato SSL.
                    Altri provvedimenti utili a prevenire il session hijacking, e simili forme di attacchi XSS sono:
                    <br>
                    <ul>
                        <li>
                            l´inserimento di una stringa  di rigenerazione del session id all’interno della funzione di validazione login utente. 
                        </li>
                        <li>
                            effettuare l'escape dei dati inseriti dall'utente tramite funzioni come l'htmlspecialchart build in di PHP(che converte in stringa i valori inseriti in input lato client
                        </li>
                        <li>
                            essendo htaccess in ambiente apache una sorta di punto di raggruppamento per configurazioni da far rispettare all’intero applicativo caricato, dovrebbe poter esistere la possibilità di inserire dei flag di sicurezza per limitare l'inserimento di codice malevolo nei cookie di sessione
                        </li>                       
                        
                    </ul>
       
                </p>
                <p class="small my-3 pb-0">
                    Un' ulteriore prassi da adottare è il puntamento verso rotte POST protette da csrf token dei form d'invio dati lato client. I dati non saranno facilmente individuabili nella head della request viaggiando nel body della stessa, accompagnati da una chiave identificativa unica della request(una prassi che in realtà tutela più che altro da attacchi relativi alla ”fiducia" di un sito nel browser di un utente, a differenza degli attacchi XSS che sfruttanola fiducia di un utente in un sito )
                </p>
            </div>

        </div>

        <div data-aos="fade-up" class="row mt-5">
            
            <div class="col-12 col-md-8 shadow bg-light mx-auto border border-dark rounded p-3">
                <p class="lead font-weight-bold h3">Un’azienda immette sul mercato un robot di nuova generazione e decide di rendere possibile l’estensione del codice sorgente ma non sa quali tecniche applicare.
                Illustrare i pattern di progetto o i principi dell’object oriented utilizzabili per rendere estensibile un codice.</p>
                <hr>
                <p class="lead h4">
                   
                    L’estendibilità del codice è di per se considerabile uno dei principi alla base della programmazione ad oggetti, assieme ad una più semplice gestione dei suoi componenti donata dalla natura modulare di un codice Object Oriented.
                    I concetti che permettono tale modularità possono essere riassunti in: <br>

                    <ul>
                        <li><span class="font-weight-bold"> classi astratte</span>, il codice sorgente o modello originario che getta le basi per la strutturazione del codice, in cui non potranno essere istanziati nuovi metodi direttamente;</li>
                        <li><span class="font-weight-bold"> le classi figlie</span>, estensioni specializzate della classe astratta che dettaglieranno alcune sue caratteristiche e funzioni incapsulandone il codice;</li>
                        <li><span class="font-weight-bold">la dependency injection</span>, ovvero la possibilità di far ereditare ad una “classe principale" le spicificità delle classi gerarchicamente superiori, create prima, rendendola di fatto un estensione del modello principale</li>
                        <li><span class="font-weight-bold">infine la creazione di un oggetto</span>, istanza dell’ultima classe considerata, che possa avvalersi di tutte le specificità, ovvero degli attributi e dei metodi, richiamati nella classe.</li>
                    </ul>
                       
                    L`applicativo qui sviluppato, utilizzando laravel come framework, fa già ampio uso dei principi accennati in precedenza per svolgere la maggior parte delle sue funzioni (i controller derivano molte delle loro proprietà da classi gerarchicamente superiori ed infine da una classe astratta Controller, stesso dicasi per i model, le migretion, le request. Anche la comunicazione tra una classe ed un model per permettere la traduzione di dati acquisiti lato client in "linguaggio" comprensibile al database è frutto di modulazione ad oggetti) ma volendo brevemente riprendere l’esempio fornito dalla traccia, e romanzandolo in ottica cartoonesca, potremmo ipotizzare la strutturazione di un robottone degno del migliore Jeeg. 
                    <br><br>
                    Questo potrebbe essere composto da due classi astratte:  ParteSuperiore, che preveda una funzione astratta attacco(), ed una ParteInferiore che preveda una funzione astratta movimento().<br>
                    Potremmo a questo punto pensare a delle classi che estendano le due classi genitore. Per la parte superiore creeremo una classe DoppioMaglioPerforante extends ParteSuperiore, istruita di un metodo, una funzione publica, attacco() che eseguirà un blocco di codice capace di trapassare il nemico in pieno busto. Potremmo anche aggiungere una seconda classe estesa della parte superiore chiamata SuperNeutroniDallOmbelico, che similmente alla classe parimenti gerarchica sarà istruita con un metodo di attacco(). Faremo lo stesso con due metodi estesi della parte inferiore(CorsaVelocissima e VoloImpressionante) entrambi istruiti di un metodo movimento().<br>
                    A questo creremo una classe Jeeg con due attributi, parte_anteriore e parte_posteriore nella cui funzione construct andrà ad accettare come parametri le due classi astratte ParteSuperiore e ParteInferiore seguite da due variabili, possibilmente aventi lo stesso nome delle classi. <br> Ora potremo richiamare un oggetto jeegrobot come nuova istanza della classe Jeeg avente per attributi delle istanze nella classe DoppioMaglioPerforante e VoloImpressionante, ad esempio. <br>
                    <div class="text-center my-3">
                        <code>jeegrobot← new Jeeg(new DoppioMaglioperforante(), new VoloImpressionante())</code>
                    </div> 
                    l’oggetto jeegrobot potrà ora avvalersi del, o dei, metodi di tutte le classi (ereditate), essendo di fatto una composizione modulare delle stesse.
                    
                    
                </p>
                    
                  
            </div>

        </div>

        <div data-aos="fade-up" class="row mt-5">
            
            <div class="col-12 col-md-8 shadow bg-light mx-auto border border-dark rounded p-3">
                <p class="lead font-weight-bold h3">Un utente lamenta di non ricevere mail sulla propria casella di posta aziendale nonostante diversi suoi contatti dicono di aver già mandato i rispettivi messaggi. Il webmaster accede al server e non trova alcuna e-mail.
                    Ipotizzare almeno una motivazione riconducibile alla configurazione di posta del dominio.</p>
                <hr>
                <p class="lead h4">
                    I servizi di hosting forniscono generalmente un collegamento a servizi di mailing interni od esterni allo stesso servizio di hosting, generalmente configurabili da interfaccia direttamente sul server. <br> La prima idea che mi verrebbe da considerare e qualche errore di configurazione a monte, come il mancato inserimento della mail stessa nel servizio di configurazione. <br>
                    Escluso il primo, e più immediato, dei dubbi andrei a controllare le configurazioni del mio applicativo assicurandomi che: l`indirizzo email venga effettivamente inviato al server, che il file di configurazione del mio applicativo col mailing (il file .env nella sezione database nel caso di laravel) sia collegato alla porta corretta del server ed abbia tutti i dati necessari alla comunicazione con l`applicativo web. 
                </p>
                    
                  
            </div>

        </div>

        <div data-aos="fade-up" class="row mt-5">
            
            <div class="col-12 col-md-8 shadow bg-light mx-auto border border-dark rounded p-3">
                <p class="lead font-weight-bold h3">Spiegare come il file .htaccess sia in grado di ottimizzare il caricamento di un sito web.</p>
                <hr>
                <p class="lead h4">
                    Nel file htaccess è possibile stabilire le direttive  per il tempo di memorizzazione nelle cache del browser dei file necessari al caricamento del sito. File come gli script js, i fogli di stile, le immagini in formato jpeg e gif, ecc. possono restare nella memoria interna del browser permettendo un caricamento più rapido del sito dopo la prima visita dell'utente, almeno per un certo periodo di tempo. Inoltre è possibile inserire la configurazione della compressione gzip per i file js css html, rendendoli più leggeri al caricamento del sito. Tutte le righe di codice necessarie alla configurazione delle due precedenti strategie sono facilmente reperibili online.
                </p>
                    
                  
            </div>

        </div>

        <div data-aos="fade-up" class="row mt-5">
            
            <div class="col-12 text-center">
                <h2 class="font-weight-bold h1">Front-end</h2>
            </div>

        </div>

        <div data-aos="fade-up" class="row mt-5">
            
            <div class="col-12 col-md-8 shadow bg-light mx-auto border border-dark rounded p-3">
                <p class="lead font-weight-bold h3">Il candidato descriva cosa sia jQuery, quali siano vantaggi e svantaggi del suo utilizzo (se ce ne sono) e se lo userebbe in un sito web motivando la risposta</p>
                <hr>
                <p class="lead h4">
                    E’ una libreria nata con l`intento di semplificare il lavoro degli sviluppatori JavaScript, dalle piccole cose come la cattura di un elemento del DOM abbreviandone la sintassi, fino a cose più complicate come l`utilizzo di metodi nativamente non presenti in JS, animazioni complesse o chiamate ajax(anche in questi ultimi casi principalmente abbreviadone la sintassi). Con gli ultimi aggiornamenti di ECMA-Script ha guadagnato maggiore efficacia grazie all’introduzione di metodi generici di richiamo dei dati dal DOM, nuovi metodi per lavorare sugli array, o altri formati supportati, di fatto colmando quel gap che rendeva jquery quasi indispensabile per la programmazione in JavaScript. Di recente il trend generale è quello di abbandonare jquery per un maggiore utilizzo di JavaScript vanilla. Persino la libreria Twitter Bootstrap, nella sua versione 5.0 ha scelto di mettere da parte gli script jquery per passare a quelli in js puro. 
                    Il rischio di adottare ancora Jquery nei propri applicativi, è quello nel breve periodo di non vedere più supportata la libreria e per tanto essere costretti ad interventi massivi quanto meno sul lato client del sito.


                </p>
                    
                  
            </div>

        </div>

        <div data-aos="fade-up" class="row mt-5">
            
            <div class="col-12 col-md-8 shadow bg-light mx-auto border border-dark rounded p-3">
                <p class="lead font-weight-bold h3">Descrivere il principio di funzionamento di una closure e a cosa è utile, argomentare con un esempio pratico, commentando il codice.</p>
                <hr>
                <p class="lead h4">
                    Una closure è un tipo di oggetto unico di JavaScript creato dal comportamento speciale del linguaggio relativamente alla cornice di validità di una variabile all’interno del suo ambito di funzione (o meglio lo scope della funzione in cui la variabile e dichiarata). Partiamo dal considerare il fatto che JavaScript legge il blocco di codice interno ad una funzione solo quando la stessa viene chiamata su di una variabile. Quando il blocco di codice viene eseguito correttamente, la funzione si chiude returnando una bag di dati contenente un solo valore o, nel caso di funzioni nidificate che non vengano richiamate a cascata, il valore di return più la funzione non ancora eseguita. Una closure, appunto, un oggetto creato dalla funzione e da ciò che resta dell’ambito in cui è stata creata. 

                     
                        <pre class="">
                            <code class="text-light border rounded vw-100">
                                function sumMkr(a) {
                                    return function(b) {
                                    return a + b;
                                };
                                }
                                
                                let sum3 = sumMkr(3);
                                let sum7 = sumMkr(7);
                                
                                console.log(add5(3));  // 6
                                console.log(add10(3)); // 10
                            </code>
                        </pre>
                       
                  
                    In questo esempio, abbiamo definito una funzione `sumMkr(a)` che prende un singolo argomento a e restituisce una nuova funzione. La funzione restituita prende un singolo argomento b, e restituisce la somma a+b.
                    In sostanza, sumMkr crea funzioni che possono aggiungere uno specifico valore come argomento. La usiamo per creare due nuove funzioni una che aggiunge 3 , e una che aggiunge 7
                    sum3 e sum10 sono entrambe closures. Condividono la stessa definizione del corpo della funzione, ma memorizzano diversi ambienti. 
                
                </p>
                    
                  
            </div>

        </div>

    </div>

    @push('gsap')


        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

        <script>
            AOS.init();
        </script>

    @endpush    


</x-layout>