//custom navbar

let navbar = document.querySelector('#navbar')
let navLogo = document.querySelector('#navLogo')
let navLink = [...document.getElementsByClassName("linkNav")]


if(window.innerHeight > 476) {
    document.addEventListener('scroll',()=> {
        

        if (window.pageYOffset > 20) {
            navbar.classList.remove('bg-transparent')
            navbar.classList.add('bg-white','shadow','fixed-top')
           
           navLink.forEach(element => {
               element.classList.add('text-dark')
               element.classList.remove('text-light')
               
           });
           
       
        }else{
            navbar.classList.remove('bg-white','shadow','fixed-top') 
            navLink.forEach(element => {
                element.classList.add('text-light')
                element.classList.remove('text-dark')              
            });
        }

    })
} 


//Script gestione header

let header = document.querySelector('#header')
let home = document.querySelector('#homepage')

if(home){

    header.classList.add('header_bg')
   

}else{

    header.classList.add('halfheader_bg')
}



//form Script
 //creo una funzione con scope globale
 window.send = ()=>{ 
      
    
    //dichiaro una nuova costante form data come nuovo oggetto della classe FormData, ma non istanzio nulla.
    const formData = new FormData();
    //Catturo gli input dal form html usando l'id e creo un nuovo formdata object(un oggetto chiave valore che permette di accettare gli input attraverso una chiamata ajax) "appendendoci" dentro i valori dell'input. uso .value per ottenere in valore effettivo dell'input e non il codice html che lo compone

        formData.append('name', nme.value);
        formData.append('surname', surname.value);
        formData.append('telephone', telephone.value);
        formData.append('email', email.value);
        formData.append('age', age.value);
        formData.append('cv', cv.files[0]); 
     
    //semplifico la chiamata ajax usando la libreria axios portata dallo scaffholding di bootstrap. Nella chiamata specifico il metodo post per l'invio del form, url della funzione store nel controller, passo l'oggetto formdata con i dati catturati dagli input, che porta con se la specifica di tipologia input 'multipart/form-data dunque permettendomi di non specificarla. La chiamata è asincrona, quindi specifico .then perchè soltanto dopo che l'invio dati sarà completato venga lanciata la funzione response che apre un alert con in stampa il return della funzione new in controller (un json con un messaggio)

    axios({ 
        method: 'post',
        url: '/store',
        data: formData,    
        // headers: {
        //     'content-type': 'multipart/form-data'
        // }
        })  
                      
    .then(function (response) {
        alert(response.data)
        let cv = document.querySelector("input[type=file]");
        cv.value = "";      
    })
    
    nme.value = ""
    surname.value = ""
    telephone.value = ""
    email.value = ""
    age.value = ""
  
  }


//   window.send = ()=>{ 
      
    
//     //dichiaro una nuova costante form data come nuovo oggetto della classe FormData, ma non istanzio nulla.
//     const formData = new FormData();
//     //Catturo gli input dal form html usando l'id e creo un nuovo formdata object(un oggetto chiave valore che permette di accettare gli input attraverso una chiamata ajax) "appendendoci" dentro i valori dell'input. uso .value per ottenere in valore effettivo dell'input e non il codice html che lo compone

//         formData.append('name', nme.value);
//         formData.append('description', description.value);
//         formData.append('link', link.value);
//         formData.append('addr', addr.value);
//         formData.append('lat', lat.value);
//         formData.append('lon', lon.value); 
//         formData.append('img', img.files[0]); 
     
//     //semplifico la chiamata ajax usando la libreria axios portata dallo scaffholding di bootstrap. Nella chiamata specifico il metodo post per l'invio del form, url della funzione store nel controller, passo l'oggetto formdata con i dati catturati dagli input, che porta con se la specifica di tipologia input 'multipart/form-data dunque permettendomi di non specificarla. La chiamata è asincrona, quindi specifico .then perchè soltanto dopo che l'invio dati sarà completato venga lanciata la funzione response che apre un alert con in stampa il return della funzione new in controller (un json con un messaggio)

//     axios({ 
//         method: 'post',
//         url: '/front/store',
//         data: formData,    
//         // headers: {
//         //     'content-type': 'multipart/form-data'
//         // }
//         })  
                      
//     .then(function (response) {
//         alert(response.data)
//         let img = document.querySelector("input[type=file]");
//         img.value = "";      
//     })
    
//     nme.value = ""
//     description.value = ""
//     link.value = ""
//     lat.value = ""
//     lon.value = ""
    
//   }