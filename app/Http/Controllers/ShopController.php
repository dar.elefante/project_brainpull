<?php

namespace App\Http\Controllers;

use App\Models\Shop;
use Illuminate\Http\Request;

class ShopController extends Controller
{

    public function map(Request $request)
    { 
         $pvd = Shop::all();
     
        return view('map', compact('pvd'));
    } 


    public function store(Request $request)
    {  
        $img = $request->file('img')->store('/img');  
           
        $pvd = Shop::create([
            'name' => $request->name,
            'description' => $request->description,
            'lon' => $request->lon,
            'lat' => $request->lat,
            'addr' => $request->addr,
            'img' => $img,
         ]);

           
         return redirect()->back()->with('notified','Il punto vendita è stato aggiunto con successo');
 
    } 
}
