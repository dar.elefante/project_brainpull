<?php

namespace App\Http\Controllers;

use App\Models\Shop;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function answer()
    {
 
     return view('answer');
 
    }

    public function front()
    {
     $pvd = Shop::all();
     
     return view('front', compact('pvd'));
 
    } 

    public function cv()
    {
 
     return view('cv');
 
    } 
}
