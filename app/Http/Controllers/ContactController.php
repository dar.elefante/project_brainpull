<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Mail\CvReceived;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function create()
    {
 
     return view('home');
 
    } 


    public function store(Request $request)
    {    
        
        // Specifico un path per inserire in store l'input file
        $cv = $request->file('cv')->store('/cv');    
         
        //I valori ricevuti dalla chiamata ajax vengono istanziati nella classe team
        $contact = Contact::create([
            'name' => $request->name,
            'surname' => $request->surname,
            'tel' => $request->telephone,
            'email' => $request->email,
            'age' => $request->age,
            'cv' => $cv,
         ]);
        //creo un nuovo oggetto della classe mailable e gli inserisco la "bag" member con i dati ottenuti dal form e salvati in database    
         $cvreceived = new CvReceived($contact);
        //uso il metodo Mail::to per inviare alla mia mail fittizia l'oggetto creato ed i suoi dati. 
         Mail::to('gino@gino.com')->send($cvreceived);
    //quando l'operazione asincrona sarà completata, invio un alert di risposta con la stampa bravo (l'alert e comandato dal .then(function()) in script.js)
     return response()->json('Il contatto è stato inviato con successo');
 
    } 
}

